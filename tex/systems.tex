\documentclass{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{stlc}
\usepackage{lib}

\begin{document}

\begin{mathparfig}{fig:stlc-types}{Types of the simply-typed calculus}
  \begin{array}{l@{~}r@{~}l@{\quad}l}
  \ta,\tb,\tc,\td & ::= &   & \mbox{types} \\
    & \mid & \aa,\ab,\ac & \mbox{atoms} \\
    & \mid & \tpa, \tpb & \mbox{positive types} \\
    & \mid & \tna, \tnb & \mbox{negative types} \\

  \tpa, \tpb & ::= & \sum{\ta}{\tb} & \mbox{strict positive} \\
  \tna, \tnb & ::= & \fun{\ta}{\tb} \mid \prod{\ta}{\tb} & \mbox{strict negative} \\

  \tapa, \tapb & ::= & \tpa, \tpb \mid \aa, \ab, \ac & \mbox{positive or atom} \\
  \tana, \tanb & ::= & \tna, \tnb \mid \aa, \ab, \ac & \mbox{negative or atom} \\
\end{array}
\end{mathparfig}

\begin{mathparfig}{fig:stlc-terms}{Terms of the lambda-calculus with sums}
  \begin{array}{l@{~}r@{~}l@{\quad}l}
  \ea,\eb,\ec & ::= &   & \mbox{terms} \\
    & \mid & \eva,\evb,\evc & \mbox{variables} \\
    & \mid & \lam{\ev}{\ea} & \mbox{$\lambda$-abstraction} \\
    & \mid & \app{\ea}{\eb} & \mbox{application} \\
    & \mid & \pair{\ea}{\eb} & \mbox{pair} \\
    & \mid & \proj{i}{\ea} & \mbox{projection ($i \in \{1,2\}$)} \\
    & \mid & \inj{i}{\ea} & \mbox{sum injection ($i \in \{1,2\}$)} \\
    & \mid & \desum{\ea}{\ev_1.\eb_1}{\ev_2.\eb_2} & \mbox{sum elimination (case split)} \\
    \\
   \ena, \enb & := &
     \eva, \evb, \evc \mid \proj{i}{\ena} \mid \app{\ena}{\ea}
     & \mbox{neutral terms} \\
\end{array}
\end{mathparfig}

\begin{mathparfig}{fig:stlc-typing-rules}{Typing rules for the simply-typed lambda-calculus}

  \infer
  {\dernat{\ca, \ev : \ta}{\e}{\tb}}
  {\dernat{\ca}{\lam{\ev}{\e}}{\fun{\ta}{\tb}}}

  \infer
  {\dernat{\ca}{\ea}{\fun{\ta}{\tb}}\\
   \dernat{\ca}{\eb}{\ta}}
  {\dernat{\ca}{\app{\ea}{\eb}}{\tb}}

  \infer
  {\dernat{\ca}{\ea}{\ta}\\ \dernat{\ca}{\eb}{\tb}}
  {\dernat{\ca}{\pair{\ea}{\eb}}{\prod{\ta}{\tb}}}

  \infer
  {\dernat{\ca}{\e}{\prod{\ta_1}{\ta_2}}}
  {\dernat{\ca}{\proj{i}{\e}}{\ta_i}}

  \infer
  {}
  {\dernat{\ca, \ev : \ta}{\ev}{\ta}}

  \infer
  {\dernat{\ca}{\e}{\ta_i}}
  {\dernat{\ca}{\inj{i}{\e}}{\sum{\ta_1}{\ta_2}}}

  \infer
  {\dernat{\ca}{\ea}{\sum{\ta_1}{\ta_2}}\\
   \dernat{\ca, \ev_1 : \ta_1}{\eb_1}{\tc}\\
   \dernat{\ca, \ev_2 : \ta_2}{\eb_2}{\tc}}
  {\dernat{\ca}{\desum{\ea}{\ev_1.\eb_1}{\ev_2.\eb_2}}{\tc}}
\end{mathparfig}

\begin{mathparfig}{fig:stlc-equiv}{$\beta\eta$-equivalence for the simply-typed lambda-calculus}
  \app{(\lam{\ev}{\ea})}{\eb} \rewbeta \subst{\eb}{\ev}{\ea}

  (\e : \fun{A}{B}) \eqeta \lam{\ev}{\app{\e}{\ev}}

  \proj{i}{\pair{\e_1}{\e_2}} \rewbeta \e_i

  (\e : \prod{A}{B}) \eqeta \pair{\proj{1}{\e}}{\proj{2}{\e}}

  \desum{\inj{i}{\ea}}{\ev_1.\eb_1}{\ev_2.\eb_2} \rewbeta \subst{\eb_i}{\ev_i}{\ea}

  \forall C[\square],\quad
  C[\e : \sum{A}{B}] \eqeta \desum{\e}{\ev.C[\inj{1}{\ev}]}{\ev.C[\inj{2}{\ev}]}
\end{mathparfig}

\begin{mathparfig}{fig:foc-natded}{Cut-free focused natural deduction for intuitionistic logic}[\mprset{andskip=1em}]
  \begin{array}{l@{~}r@{~}l@{\quad}l}
  \cn & ::= & \texttt{varmap}(\tana) & \mbox{negative or atomic context} \\
  %\cp & ::= & \texttt{varmap}(\tp) & \mbox{strictly positive context} \\
  \ca & ::= & \texttt{varmap}(\ta) & \mbox{general context} \\
  \end{array}

  \infer[inv-pair]
  {\derinv{\cn}{\ca}{\ea}{\ta}\\
   \derinv{\cn}{\ca}{\eb}{\tb}}
  {\derinv{\cn}{\ca}{\pair{\ea}{\eb}}{\prod{\ta}{\tb}}}

  \infer[inv-sum]
  {\derinv{\cn}{\ca, \ev : \ta}{\ea}{\tc}\\
   \derinv{\cn}{\ca, \ev : \tb}{\eb}{\tc}}
  {\derinv{\cn}{\ca, \ev : \sum{\ta}{\tb}}{\desum{\ev}{\ev.\ea}{\ev.\eb}}{\tc}}

  \infer[inv-arr]
  {\derinv{\cn}{\ca, \ev : \ta}{\ea}{\tb}}
  {\derinv{\cn}{\ca}{\lam{\ev}{\ea}}{\fun{\ta}{\tb}}}

  \infer[inv-end]
  {\derfoc{\cn, \cn'}{\ea}{\tap}}
  {\derinv{\cn}{\cn'}{\ea}{\tap}}

  \\

  \infer[foc-intro]
  {\derup{\cn}{\ea}{\tp}}
  {\derfoc{\cn}{\ea}{\tp}}

  \infer[foc-atom]
  {\derdown{\cn}{\en}{\aa}}
  {\derfoc{\cn}{\en}{\aa}}

  \infer[foc-elim]
  {\derdown{\cn}{\en}{\tpa}\\
   \derinv{\cn}{\ev : \tpa}{\e}{\tapb}}
  {\derfoc{\cn}{\letin{\ev}{\en}{\e}}{\tapb}}

  \infer[intro-sum]
  {\derup{\cn}{\ea}{\ta_i}}
  {\derup{\cn}{\inj{i}{\ea}}{\sum{\ta_1}{\ta_2}}}

  \infer[intro-end]
  {\derinv{\cn}{\emptyset}{\ea}{\tan}}
  {\derup{\cn}{\ea}{\tan}}

  \infer[elim-pair]
  {\derdown{\cn}{\en}{\prod{\ta_1}{\ta_2}}}
  {\derdown{\cn}{\proj{i}{\en}}{\ta_i}}

  \infer[elim-start]
  {(\ev : \tan) \in \cn}
  {\derdown{\cn}{\ev}{\tan}}

  \infer[elim-arr]
  {\derdown{\cn}{\en}{\fun{\ta}{\tb}}\\
   \derup{\cn}{\eb}{\ta}}
  {\derdown{\cn}{\app{\en}{\eb}}{\tb}}
\end{mathparfig}


\begin{mathparfig}{fig:sat-natded}{Cut-free saturating focused intuitionistic logic}

  \infer[sinv-sum]
  {\dersinv{\cn}{\ca, {\ev} : \ta}{\ea}{\tc}\\
   \dersinv{\cn}{\ca, {\ev} : \tb}{\eb}{\tc}}
  {\dersinv{\cn}{\ca, {\ev} : \sum{\ta}{\tb}}{\desum{\ev}{\ev.\ea}{\ev.\eb}}{\tc}}

  \infer[sinv-arr]
  {\dersinv{\cn}{\ca, {\ev} : \ta}{\ea}{\tb}}
  {\dersinv{\cn}{\ca}{\lam{\ev}{\ea}}{\fun{\ta}{\tb}}}

  \infer[sinv-pair]
  {\dersinv{\cn}{\ca}{\ea}{\ta}\\
   \dersinv{\cn}{\ca}{\eb}{\tb}}
  {\dersinv{\cn}{\ca}{\pair{\ea}{\eb}}{\prod{\ta}{\tb}}}

  \infer[sinv-end]
  {\dersat{\cn}{\cn'}{\ea}{\tap}}
  {\dersinv{\cn}{\cn'}{\ea}{\tap}}

  \infer[sat]
  {(\bar{\en}, \bar{\tp}) \subseteq
   \{ (\en, \tp) \mid (\derdown{\cn, \cn'}{\en}{\tp}) \wedge \reluses{\en}{\cn'} \}
   \\
   \dersinv{\cn, \cn'}{\bar{x} : \bar{\tp}}{\ea}{\tapb}
   \\
   \forall x \in \bar{x},\,\reluses{\ea}{x}
  }
  {\dersat{\cn}{\cn'}{\letin{\bar{x}}{\bar{\en}}{\ea}}{\tapb}}

  \infer[sat-intro]
  {\derup{\cn}{\ea}{\tp}}
  {\dersat{\cn}{\emptyset}{\ea}{\tp}}

  \infer[sat-atom]
  {\derdown{\cn}{\en}{\aa}}
  {\dersat{\cn}{\emptyset}{\en}{\aa}}

  \infer[intro-sum]
  {\derup{\cn}{\ea}{\ta_i}}
  {\derup{\cn}{\inj{i}{\ea}}{\sum{\ta_1}{\ta_2}}}

  \infer[intro-end]
  {\dersinv{\cn}{\emptyset}{\ea}{\tan}}
  {\derup{\cn}{\ea}{\tan}}

  \infer[elim-pair]
  {\derdown{\cn}{\en}{\prod{\ta_1}{\ta_2}}}
  {\derdown{\cn}{\proj{i}{\en}}{\ta_i}}

  \infer[elim-start]
  {({\ev} : \tan) \in \cn}
  {\derdown{\cn}{\ev}{\tan}}

  \infer[elim-arr]
  {\derdown{\cn}{\en}{\fun{\ta}{\tb}}\\
   \derup{\cn}{\eb}{\ta}}
  {\derdown{\cn}{\app{\en}{\eb}}{\tb}}

  \infer
  {\ev \in \ca}
  {\reluses{\ev}{\ca}}

  \infer
  {(\exists {\en \in \bar{\en}},\, \reluses{\en}{\ca}) \vee \reluses{\ea}{\ca}}
  {\reluses{\letin{\bar{\ev}}{\bar{\en}}{\ea}}{\ca}}

  \infer
  {(\reluses{\e_1}{\ca}) \vee (\reluses{\e_2}{\ca})}
  {\reluses{\desum{\ev}{\ev.\e_1}{\ev.\e_2}}{\ca}}

  \infer
  {(\reluses{\ea}{\ca}) \vee (\reluses{\eb}{\ca})}
  {\reluses{\app{\ea}{\eb}}{\ca}}

  (\text{other $(\reluses{\e}{\ca})$: simple or-mapping like for $\app{\ea}{\eb}$})
\end{mathparfig}


\end{document}
