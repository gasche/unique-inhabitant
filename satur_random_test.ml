(*

#directory "_build";;
#load "satur_lib.cmo";;
#load "satur.cmo";;
#load "satur_printers.cmo";;
#load "generator.cmo";;
#load "unix.cma";;

#install_printer Satur_printers.format_formula;;
#install_printer Satur_printers.format_context;;

*)

open Satur_lib

let ($) f x = f x
let ($$) = Generator.app

let random_atom =
  let open Generator in
  map (fun s -> Atom s) $ string (return 1) (make_char 'A' 10)

let fueled_formula =
  let open Generator in
  let open Fuel in
  fix (fun fueled_formula () ->
    let bin f =
      binary (fueled_formula ()) (fueled_formula ())
        (fun v1 v2 -> pure f $$ v1 $$ v2)
    in
    fuel_choose [
      nullary random_atom;
      bin (fun a b -> And (a, b));
      bin (fun a b -> Or (a, b));
      bin (fun a b -> Impl (a, b));
    ]
  ) ()

let random_formula size random =
  match fueled_formula random size with
    | None -> assert false
    | Some gen -> gen random

let test random ?output ~last_formula ~num_iter ~max_size =
  let log = ref [] in
  for i = 1 to num_iter do
    let size = 10 + (i / 200) in
    let formula = random_formula size random in
    last_formula := Some formula;
    begin match output with
      | None -> ()
      | Some chan -> 
         Printf.fprintf chan "%d: size=%d formula=\"%s\"\n%!"
           i size (Satur_printers.print_formula formula)
    end;
    let time_before = Unix.gettimeofday () in
    let result = Satur.search formula in
    let time_after = Unix.gettimeofday () in
    log := (size, formula, Satur.count result, time_after -. time_before) :: !log;
  done;
  !log

let random = Random.State.make [|0|];;
let last_formula = ref (None : formula option);;

let () =
  if not !Sys.interactive then begin
    match int_of_string Sys.argv.(1), int_of_string Sys.argv.(2) with
      | exception _ -> prerr_endline "usage: ./prog <num_iter> <max_size>"
      | (num_iter, max_size) ->
         let result =
           test random ~output:stderr ~last_formula ~num_iter ~max_size in
         let min_positive_time =
           List.fold_left (fun m (_s, _f, _c, t) ->
             if t > 0. then min m t else m
           ) 1. result in        
         result |> List.iter (fun (size, _formula, count, time) ->
           let time = if time > 0. then time else min_positive_time in
           Printf.printf "%d %f\n" size (log time)
         )
  end


(* killer formulae:

"(((((((B ∨ C) ∧ ((B ∧ C) ∧ ((E ∨ D) ∨ (G ∨ G)))) ⇒ B) ∨ ((F ∨ (H ⇒ A)) ⇒ ((E ⇒ ((I ∨ (C ⇒ G)) ∧ ((I ∨ B) ∧ G))) ∨ (((G ∧ A) ∧ (H ⇒ F)) ∨ (H ⇒ F))))) ⇒ (G ∨ ((J ∨ C) ⇒ D))) ∨ (G ∧ B)) ⇒ H)"

"(((I ∨ G) ∧ (((E ∨ (J ∧ F)) ⇒ (G ∧ ((E ∨ (D ∨ E)) ∧ (((H ⇒ F) ⇒ I) ∨ E)))) ⇒ ((C ⇒ (B ∧ J)) ∨ ((H ∨ F) ⇒ J)))) ⇒ (((H ∨ B) ⇒ (I ∧ I)) ∨ A))"

9505: size=19 formula="((((J ∧ C) ⇒ (H ∨ (F ⇒ C))) ⇒ (G ∧ (((H ∨ G) ∧ (I ⇒ ((C ∨ J) ∨ ((C ∧ H) ∧ D)))) ∧ (I ∨ C)))) ⇒ (((H ⇒ C) ∧ G) ⇒ J))"

 *)
