
let a,b,c,d = Atom "A", Atom "B", Atom "C", Atom "D";;

let id f = Impl(f, f);;

let search formula = search_inv Context.empty [] formula

let f0 = id a;;
let test0 = search f0;;

let f1 = id (Impl (b, And (c, c)));;
let test1 = search f1;;

let f2 = id (Impl (c, d));;
let test2 = search f2
