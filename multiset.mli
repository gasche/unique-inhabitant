module type OrderedType = sig
  type t
  val compare : t -> t -> int
end

module Make : functor (K : OrderedType) ->
  sig
    module M : Map.S

    type t
    type key = K.t
  
    val empty : t
    val is_empty : t -> bool
    val mem : key -> t -> bool
  
    (** returns 0 if the key isn't present in the map *)
    val find : key -> t -> int
  
    (** increases the key count by 1 *)
    val push : key -> t -> t

    (** decreases the key count by 1, raise [Not_found] if already 0 *)
    val pop : key -> t -> t 
  
    (** only iterates over present keys *)
    val fold : (key -> int -> 'acc -> 'acc) -> t -> 'acc -> 'acc
  
    val union : t -> t -> t

    val equal : t -> t -> bool

    val equal_as_sets : t -> t -> bool

    val compare_as_sets : t -> t -> int
  end
  with module M = Map.Make(K)
