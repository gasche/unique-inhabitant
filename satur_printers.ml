open Satur_lib
       
(* for toplevel usage *)
let rec print_formula = function
  | Atom x -> x
  | Or (a, b) -> print_op "∨" a b
  | And (a, b) -> print_op "∧" a b
  | Impl (a, b) -> print_op "⇒" a b
and print_op op a b =
  String.concat "" ["("; print_formula a; " "; op; " "; print_formula b; ")"]

let print_context (context : Context.t) =
  "[" ^ String.concat ", " (
     Context.fold (fun hyp multi li ->
       let multi = match multi with
           | Plur.Zero -> None
           | Plur.One n -> Some (Var.to_string n)
           | Plur.Two (n1, n2) ->
              Some Var.(Printf.sprintf "%s,%s" (to_string n1) (to_string n2)) in
       match multi with
         | None -> li
         | Some multi -> Printf.sprintf "%s:%s" multi (print_formula hyp) :: li
     ) context []
  ) ^ "]"

let formatter printer = fun buf v -> Format.fprintf buf "%s%!" (printer v)

let format_formula = formatter print_formula
let format_context = formatter print_context
