open Satur_lib

(* redundant Hyp gamma t: adding a redundant hypothesis [t] in [gamma]
                          will not tell us anything new

   redundant Goal gamma t: the goal [t] is redundant when we already have
                           enough proofs in the context,
                           no need to saturate further
*)
type redundancy_mode = Hyp | Goal
let no_redundancy =
  try ignore (Sys.getenv "NO_REDUNDANCY_CHECK"); true with _ -> false
let rec redundant mode gamma t =
  if no_redundancy then false else begin
    match t with
     | Or (a, b) ->
        begin match mode with
          | Hyp ->
             (* we know the case-split is unnecessary
                if *both* sides introduce redundant hypotheses in the context *)
             redundant mode gamma a && redundant mode gamma b
          | Goal ->
             (* a goal is redundantly proved if either sides is *)
             redundant mode gamma a || redundant mode gamma b
        end
    (* we initially call [redundant] with strictly positives only,
       but recursive subcalls may be on atoms or negatives *)
    | Atom _ ->
       (match Context.find t gamma with Plur.Two _ -> true | _ -> false)
    | And (a, b) ->
       redundant mode gamma a && redundant mode gamma b
    | Impl (_a, b) ->
       redundant mode gamma b
  end

let select_oblis p gamma =
  let add_oblis na multi oblis =
    let candidates =
      decomp na
      |> List.filter (fun (obli, head) -> p head)
      |> List.map (fun (obli, head) ->
                   multi >>= fun v ->
                   return @@ (obli (StartDown (gamma, v, na)), head)
                  )
    in candidates @ oblis
  in Context.fold add_oblis gamma []

module VarSet = Set.Make(Var)

let context_as_set context =
  Context.fold (fun _formula vars set ->
    List.fold_right VarSet.add (Plur.list vars) set) context VarSet.empty


(* Remark: there is something slightly fishy with the test of variable
   usage, as it may a priori depend on the choice of the two
   representatives in the context: if there are more than two ways to
   cut a given positives, only two terms will be retained, and it
   could be that two "new" terms (using the new context) are retained,
   or two old terms, and this means that the usage term may or may not
   keep new terms at this type depending on this choice.

   I think this is correct, because if old terms of type P throw away
   the new terms, these old terms were already saturated in a previous
   phase, and the total multiplicity will be correct:

   - if two old terms throw away all new terms (and are filtered out),
     those two old terms were cut before there was already 2-or-more
     P in the context before saturation (throwing new terms for P away
     makes no difference); those old terms may have been themselves
     thrown away from yet older terms, but we can apply this reasoning
     inductively

   - if there is only one old term, and it displaces one new term,
     then with the extra new term we have 2-or-more P in the context
     after saturation (having thrown one term makes no difference)
 *)



(* uses Σ n := fv(n) ∩ Σ ≠ ∅ *)
let uses sigma proof =
  let rec uses_inv sigma = function
    | (RAnd (da, db)
      | LOr (_, _, da, _, db))
      -> uses_inv sigma da || uses_inv sigma db
    | RImpl (_v, _a, da) -> uses_inv sigma da
    | Foc (_, _, d) -> uses_foc sigma d
  and uses_foc sigma = function
    | AtomDown d -> uses_down sigma d
    | StartUp d -> uses_up sigma d
    | Cut (bindings, d) ->
       (* [d] can use [sigma] either by using one of its variables directly,
          or using a binding that itself uses [sigma]; in other words, the bindings
          that use [sigma] should be added to the set of variables to watch for. *)
       let new_vars =
         bindings
         |> List.filter (fun (v, d) -> uses_down sigma d)
         |> List.map fst in
       let new_sigma = List.fold_right VarSet.add new_vars sigma in
       uses_inv new_sigma d
  and uses_down sigma = function
    | LAnd (d, (Left () | Right ())) -> uses_down sigma d
    | LImpl (down, up) -> uses_down sigma down || uses_up sigma up
    | StartDown (_, v, _) -> VarSet.mem v sigma
  and uses_up sigma = function
    | ROr (Left (d, _) | Right (_, d)) -> uses_up sigma d
    | EndUp d -> uses_inv sigma d
  in uses_down sigma proof

(* Δ/Γ *)
let quotient delta ~over:gamma =
  let delta =
    List.fold_left (fun m (v, a) -> Context.add a v m) Context.empty delta in
  let quoti k v =
    let open Plur in
    begin match Context.find k gamma with
      | Two _ -> Zero
      | Zero -> v
      | One _ ->
         begin match v with
                 | Two (x, _) -> One x
                 | One _ | Zero -> v
         end
    end
  in Context.M.mapi quoti delta

(* (Γ, Δ)   – with priority to Γ's variables *)
let add_after gamma delta =
  Context.M.merge (fun k v1 v2 ->
    match v1, v2 with
      | None, v | v, None -> v
      | Some (Plur.Two _), Some _ -> v1
      | Some a, Some b -> Some (a ++ b)
  ) gamma delta

module Formula = struct
  type t = formula
  let compare = compare
end

module Judgment = struct
  type t = Context.t * formula
  let compare (gamma1, a1) (gamma2, a2) =
    match compare a1 a2 with
      | 0 -> Context.compare gamma1 gamma2
      | n -> n
end

module Memory = struct
  module JMap = Map.Make(Judgment)
  type t = unit Plur.t JMap.t
  let find j mem =
    try JMap.find j mem
    with Not_found -> Plur.Zero
  let add j v mem =
    JMap.add j v mem
  let empty = JMap.empty
end

module Tabulation = struct
  module FMap = Map.Make(Formula)
  module CMap = Map.Make(Context)
  type 'a t = 'a CMap.t FMap.t
  let empty = FMap.empty
  let with_goal goal tab =
    try FMap.find goal tab with Not_found -> CMap.empty
  let find context goal tab =
    CMap.find context @@ FMap.find goal @@ tab
  let add context goal v tab =
    FMap.add goal (CMap.add context v (with_goal goal tab)) tab
end

let table_up : derivation_up Plur.t Tabulation.t ref = ref Tabulation.empty
let table_down : derivation_down Plur.t Tabulation.t ref = ref Tabulation.empty

let no_monotony = try ignore (Sys.getenv "NO_MONOTONY_CHECK"); true with _ -> false
let monotony context goal tab =
  if no_monotony then None else begin
    let result_ref = ref None in
    let return result = result_ref := Some result; raise Exit in
    try
      Tabulation.with_goal goal tab |> Tabulation.CMap.iter (fun res_context result ->
        begin match result with
          | Plur.Zero -> if Context.subset context res_context then return result
          | Plur.One _ -> if Context.equal context res_context then return result
          | Plur.Two _ -> if Context.subset res_context context then return result
        end
      ); None
    with Exit -> !result_ref
  end

(* Γ; Δ ⊢i A *)
let rec search_inv memo gamma delta goal =
  search_inv_split memo gamma [] delta goal

(* Γ; Δna, Σnpa ⊢i A *)
and search_inv_split memo gamma delta_na sigma goal =
    begin match sigma with
      | ((_, (Atom _ | And _ | Impl _)) as na) :: sigma ->
         search_inv_split memo gamma (na :: delta_na) sigma goal

      | (v, Or (a1, a2)) :: sigma ->
      (*
        Γ; Δ, A₁, Σ ⊢i C   Γ; Δ, A₂, Σ ⊢i C
        ——————————–————————————————————————
        Γ; Δ, A₁+A₂, Σ ⊢i C
      *)
         begin
           search_inv_split memo gamma delta_na ((v, a1) :: sigma) goal >>= fun d1 ->
           search_inv_split memo gamma delta_na ((v, a2) :: sigma) goal >>= fun d2 ->
           return @@ LOr (v, a1, d1, a2, d2)
         end

      | [] ->
         begin match goal with
           | And (a, b) ->
             (*
                Γ; Δna ⊢i A   Γ; Δna ⊢i B
                —————————————————————————
                Γ; Δna ⊢i A*B
             *)
             begin
               search_inv_split memo gamma delta_na [] a >>= fun da ->
               search_inv_split memo gamma delta_na [] b >>= fun db ->
               return @@ RAnd (da, db)
             end

           | Impl (a, b) ->
             (*
               Γ; Δ, A ⊢i B
               ——————————————
               Γ; Δ ⊢i A => B
              *)
              begin
                let v = Var.fresh () in
                search_inv_split memo gamma delta_na [(v, a)] b >>= fun d ->
                return @@ RImpl (v, a, d)
              end

           | (Atom _ | Or _) as pa ->
                (*
                   Γ; Δna/Γ ⊢ N
                   ————————————
                   Γ; Δna ⊢i N
                *)
              begin
                let delta_remainder = quotient delta_na ~over:gamma in
                search_foc memo gamma delta_remainder pa >>= fun d ->
                return @@ Foc (delta_na, delta_remainder, d)
              end
         end
    end

(* Γ; Δna ⊢ Pa *)
and search_foc memo gamma delta goal =
  if Context.is_empty delta
  then search_foc_right memo gamma goal
  else search_foc_left memo gamma delta goal

and search_foc_right memo gamma goal =
  let request = gamma, goal in
  match Memory.find request memo with
    | Plur.Two ((), ()) ->
       (* already two calls in memory: cut the branch *)
       Plur.Zero
    | (Plur.Zero | Plur.One ()) as calls ->
       let memo = Memory.add request Plur.(calls ++ One ()) memo in
       search_foc_right' memo gamma goal

and search_foc_right' memo gamma goal =
  begin match goal with
    | Atom atom ->
       begin
        (*
          Γ ⇓ X
          ————————
          Γ; ∅ ⊢ X
         *)
         search_down_atom memo gamma atom >>= fun d ->
         return @@ AtomDown d
       end
    | And _ | Impl _ ->
      (* unreachable, we assume the goal is positive or atomic *)
      assert false
    | (Or _) as pa ->
       begin
         (*
           Γ ⇑ P
           ————————
           Γ; ∅ ⊢ P
         *)
         search_up memo gamma pa >>= fun d ->
         return @@ StartUp d
       end
  end

and search_foc_left memo gamma delta goal =
  (*
    Γ, Δna ⇓ Δ'p using Δna   Γ, Δna; Δ'p ⊢i Pa
    ——————————————————————————————————————————
            Γ; Δna ⊢ Pa
  *)
  let gamma_delta = add_after gamma delta in
  if redundant Goal gamma_delta goal (* shortcut *)
  then begin
    search_up memo gamma_delta goal >>= fun d_up ->
    return @@ StartUp d_up
  end else begin
    let satur = saturate memo gamma_delta delta in
    let vars = List.map (fun _ -> Var.fresh ()) satur in
    let formulas, derivs = List.split satur in
    let cut = List.combine vars derivs in
    let context = List.combine vars formulas in
    search_inv memo gamma_delta context goal >>= fun d ->
    return @@ Cut (cut, d)
  end

and search_up memo gamma goal =
  match monotony gamma goal !table_up with
    | Some result -> result
    | None ->
      let result = search_up' memo gamma goal in
      table_up := Tabulation.add gamma goal result !table_up;
      result

(* Γ ⇑ A *)
and search_up' memo gamma goal =
  begin match goal with
    | Or (a1, a2) ->
      (*
        Γ ⇑ Ai
        ——————————–
        Γ ⇑ A₁ + A₂
       *)
       begin
         let left =
           search_up memo gamma a1 >>= fun d1 ->
           return @@ ROr (Left (d1, a2)) in
         let right =
           search_up memo gamma a2 >>= fun d2 ->
           return @@ ROr (Right (a1, d2)) in
         left ++ right
       end
    | (Atom _ | And _ | Impl _) as na ->
      (*
         Γ; ∅ ⊢i Na
         ——————————
         Γ ⇑ Na
       *)
       begin
         search_inv memo gamma [] na >>= fun d ->
         return @@ EndUp d
       end
  end

and search_down_atom memo gamma atom =
  let goal = (Atom atom) in
  match monotony gamma goal !table_down with
    | Some result -> result
    | None ->
      let result = search_down_atom' memo gamma atom in
      table_down := Tabulation.add gamma goal result !table_down;
      result

(* Γ ⇓ X *)
and search_down_atom' memo gamma x =
  let oblis = select_oblis (function (Atom y) -> x=y | _ -> false) gamma in
  let proofs acc multi_obli =
    lazy_sum acc (fun () ->
                  multi_obli >>= fun (obli, _head) ->
                  fulfill (search_up memo) gamma obli)
  in List.fold_left proofs Plur.Zero oblis

(* Γ; Δna ⇓ Δ'p *)
and saturate memo gamma delta =
  (*
   Σ = { Γ ⇓ n : P | fv(n) ∩ Δ ≠ ∅ }
   ===================================================
            Γ ⇓ Σ using Δ
   *)
  let strictly_positive = function
    | Or _ -> true
    | Atom _ | And _ | Impl _ -> false in
  (* not-cutting redundants is not necessary for soudness or completeness,
     but merely an optimization *)
  let good_cut t = strictly_positive t && not (redundant Hyp gamma t) in
  let oblis = select_oblis good_cut gamma in
  let delta_as_set = context_as_set delta in
  oblis
  |> List.map (fun multi_obli ->
    multi_obli >>= fun (obli, head) ->
    fulfill (search_up memo) gamma obli >>= fun deriv ->
    return (head, deriv))
  |> List.map Plur.list
  |> List.flatten
  |> List.filter (fun (_head, deriv) -> uses delta_as_set deriv)

let search formula = search_inv Memory.empty Context.empty [] formula
let count = function
  | Plur.Zero -> 0
  | Plur.One _ -> 1
  | Plur.Two _ -> 2
