module Var : sig
  type t = private int
  val fresh : unit -> t
  val equal : t -> t -> bool
  val compare : t -> t -> int
  val to_string : t -> string
end = struct
  type t = int
  let count = ref 0
  let fresh () = incr count; !count
  let equal : t -> t -> bool = (=)
  let compare : t -> t -> int = compare
  let to_string = string_of_int
end

module Plur = struct
  type 'a t =
  | Zero
  | One of 'a
  | Two of 'a * 'a

  let return x = One x

  let (++) r1 r2 = match r1, r2 with
    | _, Zero -> r1
    | Zero, _ -> r2
    | (One x | Two (x, _)), (One y | Two (y, _)) -> Two (x, y)

  let (>>=) m f = match m with
    | Zero -> Zero
    | One x -> f x
    | Two (x, y) -> f x ++ f y

  let equal eq v1 v2 = match v1, v2 with
      | Zero, Zero -> true
      | Zero, _ | _, Zero -> false
      | One x, One y -> eq x y
      | One _, _ | _, One _ -> false
      | Two (x1, y1), Two (x2, y2) -> eq x1 x2 && eq y1 y2

  let compare cmp v1 v2 = match v1, v2 with
      | Zero, Zero -> 0
      | Zero, _ -> -1
      | _, Zero -> +1

      | One x, One y -> cmp x y
      | One _, _ -> -1
      | _, One _ -> +1

      | Two (x1, y1), Two (x2, y2) ->
         let d = cmp x1 x2 in
         if d <> 0 then d else cmp y1 y2

  let list = function
    | Zero -> []
    | One x -> [x]
    | Two (x, y) -> [x; y]
end

let return, (>>=), (++) = Plur.(return, (>>=), (++))

(* note that lazy_sum does not have the same ordering properties as (++),
   as it avoids forcing the right-hand-side *)
let lazy_sum m thunk = match m with
    | Plur.Two _ -> m
    | _ -> m ++ thunk ()

type ('a, 'b) sum = Left of 'a | Right of 'b
let ($) f x = f x

type atom = string

type formula =
  | Atom of atom
  | And of formula * formula
  | Or of formula * formula
  | Impl of formula * formula

module Context : sig
  module M : Map.S with type key = formula

  type key = formula
  type value = Var.t Plur.t
  type t = value M.t

  val empty : t
  val is_empty : t -> bool
  val mem : key -> t -> bool

  (** returns Zero if the key isn't present in the map *)
  val find : key -> t -> value

  val add : key -> Var.t -> t -> t

  val fold : (key -> value -> 'acc -> 'acc) -> t -> 'acc -> 'acc

  val (++) : t -> t -> t

  val equal : t -> t -> bool

  val compare : t -> t -> int

  val subset : t -> t -> bool
end = struct
  module M = Map.Make(struct
                       type t = formula
                       let compare = compare
                     end)

  type key = formula
  type value = Var.t Plur.t
  type t = value M.t

  let empty = M.empty
  let is_empty = M.for_all (fun _k v -> v = Plur.Zero)

  let mem k t =
    try M.find k t <> Plur.Zero with Not_found -> false

  let find k t =
    try M.find k t with Not_found -> Plur.Zero

  let add k v t = M.add k (find k t ++ Plur.One v) t

  let fold f t = M.fold f t

  let (++) m n =
    let merge _ a b = match a, b with
        | None, None -> None
        | (Some _ as v), None | None, (Some _ as v) -> v
        | Some a, Some b -> Some (Plur.(++) a b)
    in M.merge merge m n

  let normalize m =
    M.filter (fun _ z -> z <> Plur.Zero) m

  let equal m1 m2 =
    M.equal (Plur.equal Var.equal) (normalize m1) (normalize m2)
  let compare m1 m2 =
    M.compare (Plur.compare Var.compare) (normalize m1) (normalize m2)

  let subset m1 m2 =
    try
      M.iter (fun formula mult1 ->
            let open Plur in
            let locally =
              match mult1, find formula m2 with
                | Zero, _ -> true
                | One _, (One _ | Two _) -> true
                | Two _, Two _ -> true
                | One _, Zero -> false
                | Two _, (Zero | One _) -> false
            in if not locally then raise Exit
       ) m1; true
    with Exit -> false
end

type var = Var.t
type context = Context.t
type context_list = (var * formula) list

type derivation_inv =
  | RAnd of derivation_inv * derivation_inv
  (*
    Γ; Δ ⊢i A   Γ; Δ ⊢i B
    ———————————————————
       Γ; Δ ⊢i A*B
  *)

  | LOr of var * formula * derivation_inv * formula * derivation_inv
  (*
    Γ; Δ, x:A₁ ⊢i C   Γ; Δ, x:A₂ ⊢i C
    ——————————–——————————————————————
       Γ; Δ, x:A₁+A₂ ⊢i C
  *)

  | RImpl of var * formula * derivation_inv
  (*
    Γ; Δ, x:A ⊢i B
    ——————————————
    Γ; Δ ⊢i A => B
  *)

  | Foc of context_list * Context.t * derivation_foc
  (*
     Γ; Δna/Γ ⊢ P
     ————————————
     Γ; Δna ⊢i P
  *)

and derivation_foc =
  | AtomDown of derivation_down
  (*
    Γ ⇓ X
    ————————
    Γ; ∅ ⊢ X
   *)

  | StartUp of derivation_up
  (*
    Γ ⇑ P
    ————————
    Γ; ∅ ⊢ P
  *)

  | Cut of (var * derivation_down) list * derivation_inv
  (*
   Γ, Δna ⇓ Δ'p using Δna   Γ, Δna; Δ'p ⊢i Pa
   ——————————————————————————————————————————
           Γ; Δna ⊢ Pa

   Σ = { Γ ⇓ n : P | fv(n) ∩ Δ ≠ ∅ }
   ===================================================
            Γ ⇓ Σ using Δ

   N.B.: the definition of (Γ ⇓ Σ using Δ)
   implies that (Γ ⇓ ∅ using ∅)
  *)

and derivation_down =
  | LAnd of derivation_down * (unit, unit) sum
  (*
    Γ ⇓ A₁*A₂
    —————————
    Γ ⇓ Ai
  *)

  | LImpl of derivation_down * derivation_up
  (*
    Γ ⇓ A→B    Γ ⇑ A
    ————————————————
        Γ ⇓ B
  *)

  | StartDown of context * var * formula
  (*
    Γ ∋ Na
    ——————
    Γ ⇓ Na
  *)

and derivation_up =
  | ROr of (derivation_up * formula, formula * derivation_up) sum
  (*
      Γ ⇑ Ai
    ——————————–
    Γ ⇑ A₁ + A₂
  *)

  | EndUp of derivation_inv
  (*
     Γ; ∅ ⊢i Na
     ——————————
     Γ ⇑ Na
   *)

type obligation =
  | Done of derivation_down
  | Request of formula * (derivation_up -> obligation)

let rec decomp : formula -> ((derivation_down -> obligation) * formula) list =
  function
  | (Atom _ | Or _) as pa -> [(fun d -> Done d), pa]
  | And (a1, a2) ->
     let prepend side (obli, head) = (fun d -> obli (LAnd (d, side))), head in
     List.map (prepend (Left ())) (decomp a1)
     @ List.map (prepend (Right ())) (decomp a2)
  | Impl (a, b) ->
     let prepend (obli, head) =
       (fun dab -> Request (a, fun da -> obli (LImpl (dab, da)))), head in
     List.map prepend (decomp b)

let rec fulfill search_up gamma obli =
  begin match obli with
    | Done deriv -> return deriv
    | Request (formula, req) ->
       search_up gamma formula >>= fun d_up ->
       fulfill search_up gamma (req d_up)
  end


type term =
  | Var of var
  | Lam of var * formula * term
  | App of term * term
  | Pair of term * term
  | Proj of bool * term
  | Inj of bool * term
  | Case of var * term * term
  | Let of (var * term) list * term

let rec term_inv = function
  | RAnd (da, db) -> Pair (term_inv da, term_inv db)
  | LOr (v, _ta, da, _tb, db) -> Case (v, term_inv da, term_inv db)
  | RImpl (v, f, d) -> Lam (v, f, term_inv d)
  | Foc (_delta_li, _delta, d) -> term_foc d
and term_foc = function
  | AtomDown d -> term_down d
  | StartUp d -> term_up d
  | Cut (bindings, d) ->
     let bindings = bindings |> List.map (fun (v, d) -> v, term_down d) in
     let body = term_inv d in
     if bindings = [] then body
     else Let (bindings, body)
and term_down = function
  | LAnd (d, side) ->
     let side = match side with Left () -> true | Right () -> false in
     Proj (side, term_down d)
  | LImpl (dab, da) -> App (term_down dab, term_up da)
  | StartDown (_gamma, var, _t) -> Var var
and term_up = function
  | ROr side ->
     let side, d = match side with
         | Left (d, _) -> true, d
         | Right (_, d) -> false, d
     in Inj (side, term_up d)
  | EndUp d -> term_inv d
