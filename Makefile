all:
	ocamlbuild -tag debug -lib unix satur_test.byte
	ocamlbuild -tag debug -lib unix satur_random_test.byte
	ocamlbuild -tag debug -lib unix satur_random_test.native

clean:
	ocamlbuild -clean
