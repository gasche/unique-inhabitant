module type OrderedType = sig
  type t
  val compare : t -> t -> int
end

module Make (K : OrderedType) = struct
  module M = Map.Make(K)
  type t = int M.t
  type key = K.t
  let empty = M.empty
  let is_empty = M.is_empty
  let mem = M.mem
  let find k m = try M.find k m with Not_found -> 0

  let push k m = M.add k (find k m + 1) m
  let pop k m =
    try
      match M.find k m - 1 with
        | 0 -> M.remove k m
        | n -> M.add k n m
    with Not_found as exn -> raise exn

  let fold = M.fold
  let union ma mb =
    let get = function None -> 0 | Some n -> n in
    M.merge (fun _k va vb -> Some (get va + get vb)) ma mb

  let equal ma mb = M.equal (fun (x:int) y -> x=y) ma mb

  let project = function
    | 0 -> false
    | _ -> true

  let equal_as_sets ma mb =
    M.equal (fun x y -> project x = project y) ma mb

  let compare_as_sets ma mb =
    M.compare (fun x y -> compare (project x) (project y)) ma mb
end
