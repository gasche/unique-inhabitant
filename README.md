This is a research prototype for an algorithm that decides the following question:
  Which types are uniquely inhabited?

The type system considered is the simply-typed lambda-calculus (STLC)
with sums, and the notion of equivalence (which determines the notion
of unicity) is the βη-equivalence – the natural notion of equivalence
for pure programs in this type system.

For the theory behind the implementation, see the corresponding
[article draft](http://gallium.inria.fr/~scherer/research/unique_inhabitants/unique_stlc_sums-long.pdf).

This implementation was tested to work using OCaml version
4.02.3. (It is not compatible with versions 4.01.x and below.)
