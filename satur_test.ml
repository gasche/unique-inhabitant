
(*

#directory "_build";;
#load "satur_lib.cmo";;
#load "satur.cmo";;
#load "satur_printers.cmo";;

#install_printer Satur_printers.format_formula;;
#install_printer Satur_printers.format_context;;

*)

open Satur_lib
open Satur

let count = function
  | Plur.Zero -> 0
  | Plur.One _ -> 1
  | Plur.Two _ -> 2

let terms t = Plur.list t |> List.map Satur_lib.term_inv

let a,b,c,d = Atom "A", Atom "B", Atom "C", Atom "D";;
let a',b',c',d' = Atom "A'", Atom "B'", Atom "C'", Atom "D'";;

let (@->) a b = Impl (a, b)
let ( ** ) a b = And (a, b)
let (+) a b = Or (a, b)

let id f = f @-> f

(* zero *)
let f0 = a @-> b;;
let test0 = search f0;;
let terms0 = terms test0;;
let () = assert (List.length terms0 = 0);;

(* two *)
let f1 = id (b @-> c ** c);;
let test1 = search f1;;
let terms1 = terms test1;;
let () = assert (List.length terms1 = 2);;

(* one *)
let f2 = id (c @-> d);;
let test2 = search f2;;
let terms2 = terms test2;;
let () = assert (List.length terms2 = 1);;


(* two *)
let f3 = c @-> c @-> c;;
let test3 = search f3;;
let terms3 = terms test3;;
let () = assert (List.length terms3 = 2);;

(* one *)
let f4 = a + b @-> b + a;;
let test4 = search f4;;
let terms4 = terms test4;;
let () = assert (List.length terms4 = 1);;

(* two *)
let f5 = a @-> a + a;;
let test5 = search f5;;
let terms5 = terms test5;;
let () = assert (List.length terms5 = 2);;

(* one *)
let f6 = (a @-> b) ** (b @-> c) @-> a @-> c;;
let test6 = search f6;;
let terms6 = terms test6;;
let () = assert (List.length terms6 = 1);;



(* now for some tests with non-trivial positives *)

(* one *)
let f7 = a @-> (a @-> b + c) @-> b + c;;
let test7 = search f7;;
let terms7 = terms test7;;
let () = assert (List.length terms7 = 1);;

(* one *)
let f8 = a @-> (a @-> b + b) @-> (b + c);;
  (* note: this one is not negatively non-duplicated *)
let test8 = search f8;;
let terms8 = terms test8;;
let () = assert (List.length terms8 = 1);;

(* zero *)
let f9 = a @-> (a @-> b + b) @-> c + c;;
let test9 = search f9;;
let terms9 = terms test9;;
let () = assert (List.length terms9 = 0);;

(* two *)
let f10 = a @-> (a @-> b + b) @-> b + b;;
let test10 = search f10;;
let terms10 = terms test10;;
let () = assert (List.length terms10 = 2);;

(* continuations *)
let cont r a = (a @-> r) @-> r

let r = Atom "r"

(* two *)
let cont_id = id (cont r a);;
let test_cont_id = search cont_id;;
let terms_cont_id = terms test_cont_id;;
let () = assert (List.length terms_cont_id = 2);;

(* two *)
let cont_bind = (a @-> cont r b) @-> cont r a @-> cont r b;;
let test_cont_bind = search cont_bind;;
let terms_cont_bind = terms test_cont_bind;;
let () = assert (List.length terms_cont_bind = 2);;


(* swap *)
let swap = (a @-> b @-> r) @-> (b @-> a @-> r);;

let test_swap = search swap;;
let terms_swap = terms test_swap;;
let () = assert (List.length terms_swap = 1);;


(* map-sum *)
let map_sum = (a @-> b) @-> (c @-> d) @-> a + c @-> b + d;;
let test_map_sum = search map_sum;;
let terms_map_sum = terms test_map_sum;;
let () = assert (List.length terms_map_sum = 1);;

(* church numerals *)
let church = a @-> (a @-> a) @-> a;;
let test_church = search church;;
let terms_church = terms test_church;;
let () = assert (List.length terms_church = 2);;

(** Haskell Prelude
   http://hackage.haskell.org/package/base-4.7.0.2/docs/Prelude.html *)

(* additional aliases to look closer to Haskell types *)
let (!)(a, b) = And (a, b)
let either a b = Or (a, b)

let void = Atom "void"
let one = Atom "one"

let maybe a = Or (a, one)

(* two *)
let _maybe = (one @-> b @-> (a @-> b) @-> maybe a @-> b)
let test_maybe = search _maybe |> count
let () = assert (test_maybe = 2);;

(* either :: (a -> c) -> (b -> c) -> Either a b -> c *)
(* one *)
let _either = (a @-> c) @-> (b @-> c) @-> either a b @-> c
let test_either = search _either |> count
let () = assert (test_either = 1);;

(*
fst :: (a, b) -> a
snd :: (a, b) -> b
curry :: ((a, b) -> c) -> a -> b -> c
uncurry :: (a -> b -> c) -> (a, b) -> c
*)

let count_dict d = d |> List.map (fun (name, t) -> name, count (search t))
let unique_dict d = d |> List.for_all (fun (name, num) -> num=1)


let pairs = [
  "fst", !(a, b) @-> a;
  "snd", !(a, b) @-> b;
  "curry", (!(a, b) @-> c) @-> a @-> b @-> c;
  "uncurry", (a @-> b @-> c) @-> !(a, b) @-> c;
]
let test_pairs = pairs |> count_dict;;
let () = assert (unique_dict test_pairs)

(** Haskell Control.Arrow:
  https://hackage.haskell.org/package/base-4.7.0.2/docs/Control-Arrow.html#t:Arrow

we check that (a -> b) is a valid arrow instance, and that the
required methods -- and obtained functions are uniquely determined *)

(*
class Category cat where
    id :: cat a a
    (.) :: cat b c -> cat a b -> cat a c
*)
let category_methods cat a b c = [
  "id", cat a a;
  "(.)", cat b c @-> cat a b @-> cat a c;
]

let test_category_methods =
  category_methods (@->) a b c
  |> count_dict
let () = assert (unique_dict test_category_methods)

(*
class Arrow (a : * -> * -> * ) where
  arr :: (b -> c) -> a b c
  first :: a b c -> a (b, d) (c, d)
  second :: a b c -> a (d, b) (d, c)
  (***) :: a b c -> a b' c' -> a (b, b') (c, c')
  (&&&) :: a b c -> a b c' -> a b (c, c')
*)
let arrow_methods a b c d b' c' = [
  "arr", (b @-> c) @-> a b c;
  "first", a b c @-> a !(b, d) !(c, d);
  "second", a b c @-> a !(d, b) !(d, c);
  "(***)", a b c @-> a b' c' @-> a !(b, b') !(c, c');
  "(&&&)", a b c @-> a b c' @-> a b !(c, c');
]

let test_arrow_methods =
  arrow_methods (@->) b c d b' c'
  |> count_dict
let () = assert (unique_dict test_arrow_methods)

let combinators a a' cat b c d = [
  "returnA", a b b;
  "(^>>)", (b @-> c) @-> a c d @-> a b d;
  "(>>^)", a b c @-> (c @-> d) @-> a b d;
  "(>>>)", cat a' b @-> cat b c @-> cat a' c;
  "(<<<)", cat b c @-> cat a' b @-> cat a' c;
]

let test_combinators_on_arrow =
  combinators (@->) a' (@->) b c d
  |> count_dict
let () = assert (unique_dict test_combinators_on_arrow)

(*
  class Arrow a => ArrowChoice a where
    left :: a b c -> a (Either b d) (Either c d)
    right :: a b c -> a (Either d b) (Either d c)
    (+++) :: a b c -> a b' c' -> a (Either b b') (Either c c')
    (|||) :: a b d -> a c d -> a (Either b c) d
*)
let arrowchoice_methods a b b' c c' d = [
  "left", a b c @-> a (either b d) (either c d);
  "right", a b c @-> a (either d b) (either d c);
  "(+++)", a b c @-> a b' c' @-> a (either b b') (either c c');
  "(|||)", a b d @-> a c d @-> a (either b c) d;
]

let test_arrowchoice_methods =
  arrowchoice_methods (@->) b b' c c' d
  |> count_dict
let () = assert (unique_dict test_arrowchoice_methods)


(** Lenses
  https://github.com/ekmett/lens/wiki/Types
 *)

let lens s t a b = s @-> !(a, b @-> t)
let getter s a = s @-> a
let setter s t a b = (a @-> b) @-> s @-> t
let iso s t a b = !(s @-> a, b @-> t)

let indexed_lens i s t a b =
  s @-> !(!(i, a), b @-> t)
let indexed_getter i s a =
  s @-> !(i, a)

let indexed_setter i s t a b =
  (!(i, a) @-> b) @-> s @-> t

let partial_lens s t a b =
  s @-> either t !(a, b @-> t)

let indexed i a b = i @-> a @-> b

let i, j, s, t, r = Atom "i", Atom "j", Atom "s", Atom "t", Atom "r"

(*
http://ekmett.github.io/lens/Control-Lens-Indexed.html
(<.) :: Indexable i p => (Indexed i s t -> r) -> ((a -> b) -> s -> t) -> p a b -> r
 *)
let test_lens_1 =
  let p = indexed i in
  (indexed i s t @-> r) @-> ((a @-> b) @-> s @-> t) @-> p a b @-> r
  |> search |> count
let () = assert (test_lens_1 = 1)

(*
http://ekmett.github.io/lens/Control-Lens-Indexed.html
(<.>) :: Indexable (i, j) p => (Indexed i s t -> r) -> (Indexed j a b -> s -> t) -> p a b -> r
*)
let test_lens_2 =
  let p = indexed !(i, j) in
  ((indexed i s t @-> r) @-> (indexed j a b @-> s @-> t) @-> p a b @-> r)
  |> search |> count
let () = assert (test_lens_2 = 1)

(*
http://ekmett.github.io/lens/Control-Lens-Setter.html
(%@~) :: AnIndexedSetter i s t a b -> (i -> a -> b) -> s -> t
*)
let test_lens_3 =
  indexed_setter i s t a b @-> (i @-> a @-> b) @-> s @-> t
  |> search |> count
let () = assert (test_lens_3 = 1)

(*
http://ekmett.github.io/lens/Control-Lens-Iso.html
non :: Eq a => a -> Iso' (Maybe a) a
*)

let iso' s a = iso s s a a
let test_lens_4 =
  (* several choices ! *)
  (
    one @-> (a @-> a @-> either one one)
    @-> a @-> iso' (maybe a) a
  ) |> search |> terms
let () = assert (List.length test_lens_4 = 2)

(** Either
http://hackage.haskell.org/package/base-4.7.0.2/docs/Data-Either.html
**)
let functor_requirements f a b = [
  "fmap", (a @-> b) @-> f a @-> f b;
]

let monad_requirements m a b = [
  "return", a @-> m a;
  "(>>=)", (a @-> m b) @-> m a @-> m b;
]

let applicative_requirements f a b = [
  "pure", a @-> f a;
  "(<*>)", f (a @-> b) @-> f a @-> f b;
]

let e = Atom "e"

let test_either_functor =
  functor_requirements (either e) a b
  |> count_dict
let () = assert (unique_dict test_either_functor)

let test_either_monad =
  monad_requirements (either e) a b
  |> count_dict
let () = assert (unique_dict test_either_monad)
(* Remark: bind expands to
     (A -> E+B) -> E+A -> E+B
   in (A -> E+B) it has a positive wrapped inside a negative,
   which is the case that simple focusing cannot handle:

    the bind of the exception monad justifies working with sums
 *)

let test_either_applicative =
  applicative_requirements (either e) a b
  |> count_dict
let () = assert (test_either_applicative = [("pure", 1); ("(<*>)", 2)])
(*
  Applicative on (either e) is actually not canonical!
 *)

let terms_either_ap =
  either e (a @-> b) @-> either e a @-> either e b
  |> search |> terms
let () = assert (List.length terms_either_ap = 2)
(*
Lam (7, (e ∨ (A ⇒ B)), Case (7,
    Lam (8, (e ∨ A), Case (8,
      Inj (true, Var 8),
      Inj (true, Var 7))),
    Lam (10, (e ∨ A), Case (10,
      Inj (true, Var 10),
      Inj (false, App (Var 7, Var 10)))))
)

Lam (7, (e ∨ (A ⇒ B)), Case (7,
  Lam (8, (e ∨ A), Case (8,
    Inj (true, Var 7),
    Inj (true, Var 7))),
  Lam (10, (e ∨ A), Case (10,
    Inj (true, Var 10),
    Inj (false, App (Var 7, Var 10))))
))
*)

(*
  More generally, there are *two* laws-abiding ways to convert any
  monad into an applicative functor, so applicative functors for types
  that are also monads are never canonical.

  ap :: Monad m => m (a -> b) -> m a -> m b

  ap1 mf ma = do
    f <- mf
    a <- ma
    return (f a)

  ap2 mf ma = do
    a <- ma
    f <- mf
    return (f a)

  (For (either e) this corresponds to the choice of which exception
   to return if both the function and the arguments fail.)
*)
